#version 150
in vec2 inPosition; // input from the vertex buffer

uniform mat4 viewLight;
uniform mat4 projLight;
uniform int mode;
uniform float moveZ;

const float PI = 3.1415;

vec3 getWall2(vec2 xy ) {
    return vec3(xy, moveZ); // posuneme po ose "z" o 1
}

vec3 getWallNormal2(vec2 xy) {
    vec3 u = getWall2(xy + vec2(0.001, 0)) - getWall2(xy - vec2(0.001, 0));
    vec3 v = getWall2(xy + vec2(0, 0.001)) - getWall2(xy - vec2(0, 0.001));
    return cross(u, v);
}

// ohnutí gridu do podoby elipsoidu
vec3 getSphere(vec2 xy) {
    float az = xy.x * PI;
    float ze = xy.y * PI/2; // máme od -1 do 1 a chceme od -PI/2 do PI/2
    float r = 1;

    float x = cos(az)*cos(ze)*r;
    float y = 2*sin(az)*cos(ze)*r;
    float z = 0.5*sin(ze)*r;
    return vec3(x, y, z);
}

vec3 getSphereMoveKoule(vec2 xy) {
    float t = ((xy.x+1))/2 *PI; // 0 - PI
    float s = (xy.y+1) *PI;     // 0 - 2PI

    //sfericke souradnice
    float rho = 1;
    float phi = t;
    float theta = s;

    //prevod sferickych souradnic na kartezske
    float x = rho*sin(theta)*cos(phi);
    float y = rho*sin(theta)*sin(phi);
    float z = rho*cos(theta);
    return vec3(x, y, z);
}

void main() {
    vec2 pos = inPosition * 2 - 1; // máme od 0 do 1 a chceme od -1 do 1 (funkce pro ohyb gridu s tím počítají)
    vec3 finalPos;
    if (mode == 0) { // mode 0 je stínící plocha
        finalPos = vec3(pos, 1.0); // posuneme po ose "z" o 1
        gl_Position = projLight * viewLight * vec4(finalPos, 1.0);

    } else if(mode == 1) { // mode 1 je koule
        finalPos = getSphere(pos);
        gl_Position = projLight * viewLight * vec4(finalPos, 1.0);

    } else if(mode==2) {
        finalPos = vec3(pos, moveZ);
        gl_Position = projLight * viewLight * vec4(finalPos, 1.0);

    } else if(mode==4) {
        finalPos = vec3(pos, -0.5);
        gl_Position = projLight * viewLight * vec4(finalPos, 1.0);

    }
} 
