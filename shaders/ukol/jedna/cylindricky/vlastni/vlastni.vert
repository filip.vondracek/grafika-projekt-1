#version 150
in vec2 inPosition; // input from the vertex buffer
out vec3 vertColor; // output from this shader to the next pipeline stage
uniform float time; // variable constant for all vertices in a single draw

uniform mat4 proj;
uniform mat4 view;

const float PI = 3.14;

vec3 getCoordinates(vec2 xy, float time) {
    float t = xy.x; // -1 - 1
    float s = (xy.y*5) *PI; // -5*PI - 5PI

    //cylindricke souradnice
    float r = 5;
    float theta = s*cos(5);
    float z = t;

    //prevod cylindrickych souradnic na kartezske
    float x = r*cos(theta);
    float y = r*sin(theta);
    float z2 = z;
    return vec3(x, y, z2);
}

void main() {
    vec2 pos = inPosition * 2 - 1;
    vec3 coordinates = getCoordinates(pos, time);
    vertColor = coordinates;
    gl_Position = proj * view * vec4(coordinates, 3.0);
} 
