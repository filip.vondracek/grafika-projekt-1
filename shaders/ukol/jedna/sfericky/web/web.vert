#version 150
in vec2 inPosition; // input from the vertex buffer
out vec3 vertColor; // output from this shader to the next pipeline stage
uniform float time; // variable constant for all vertices in a single draw

uniform mat4 proj;
uniform mat4 view;

const float PI = 3.14;

vec3 getCoordinates(vec2 xy) {
    float t = ((xy.x+1))/2 *PI; // 0 - PI
    float s = (xy.y+1) *PI;     // 0 - 2PI

    //sfericke souradnice
    float rho = 1;
    float phi = t;
    float theta = s;

    //prevod sferickych souradnic na kartezske
    float x = rho*sin(theta)*cos(phi);
    float y = rho*sin(theta)*sin(phi);
    float z = rho*cos(theta);
    return vec3(x, y, z);
}

void main() {
    vec2 pos = inPosition * 2 - 1;
    vec3 coordinates = getCoordinates(pos);
    vertColor = coordinates;
    gl_Position = proj * view * vec4(coordinates, 1.0);
} 
