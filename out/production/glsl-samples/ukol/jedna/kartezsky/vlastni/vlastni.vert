#version 150
in vec2 inPosition; // input from the vertex buffer
out vec3 vertColor; // output from this shader to the next pipeline stage
uniform float time; // variable constant for all vertices in a single draw

uniform mat4 proj;
uniform mat4 view;

const float PI = 3.14;

vec3 getSphere(vec2 xy, float time) {
    float s = xy.x; // -1 - 1
    float t = xy.y; // -1 - 1

    float x = t*cos(s)*cos(time);
    float y = t*sin(s);
    float z = t;
    return vec3(x, y, z);
}

void main() {
    vec2 pos = inPosition * 2 - 1;
    vec3 sphere = getSphere(pos, time);
    vertColor = sphere;
    gl_Position = proj * view * vec4(sphere, 1.0);
} 
