#version 150
in vec2 inPosition; // input from the vertex buffer
out vec3 vertColor; // output from this shader to the next pipeline stage
uniform float time; // variable constant for all vertices in a single draw

uniform mat4 proj;
uniform mat4 view;

const float PI = 3.14;

vec3 getCoordinates(vec2 xy) {
    float t = xy.y*2; // -2 - 2
    float s = (xy.x+1) *PI; // 0 - 2PI

    //cylindricke souradnice
    float r = 1;
    float theta = s;
    float z = t;

    //prevod cylindrickych souradnic na kartezske
    float x = r*cos(theta);
    float y = r*sin(theta);
    float z2 = z;
    return vec3(x, y, z2);
}

void main() {
    vec2 pos = inPosition * 2 - 1;
    vec3 coordinates = getCoordinates(pos);
    vertColor = coordinates;
    gl_Position = proj * view * vec4(coordinates, 1.0);
} 
